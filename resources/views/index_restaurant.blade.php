<!DOCTYPE html>
<html lang="en-US" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--  
    Document Title
    =============================================
    -->
    <title>Titan | Multipurpose HTML5 Template</title>
    <!--  
    Favicons
    =============================================
    -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/images/favicons/apple-icon-57x57.png') }}" ">
<!--     <link rel="apple-touch-icon" sizes="60x60" href="assets/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
 -->    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/images/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--  
    Stylesheets
    =============================================
    
    -->
    <!-- Default stylesheets-->
    <link href= "{{ asset('assets/lib/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Template specific stylesheets-->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="{{ asset('assets/lib/animate.css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/components-font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/et-line-font/et-line-font.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/flexslider/flexslider.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/owl.carousel/dist/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/owl.carousel/dist/assets/owl.theme.default.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/magnific-popup/dist/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/simple-text-rotator/simpletextrotator.css') }}" rel="stylesheet">
    <!-- Main stylesheet and color file-->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link id="color-scheme" href="assets/css/colors/default.css') }}" rel="stylesheet">
  </head>
  <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
    <main>
      <div class="page-loader">
        <div class="loader">Loading...</div>
      </div>
      <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="{{url('/') }}">UNNAMED</a>
          </div>
          <div class="collapse navbar-collapse" id="custom-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Features</a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#alerts-and-wells.html"><i class="fa fa-bolt"></i>Archery</a></li>
                  <li><a href="#buttons.html"><i class="fa fa-link fa-sm"></i>Graffiti </a></li>
                  <li><a href="#tabs_and_accordions.html"><i class="fa fa-tasks"></i>Ping pong &amp; Pool</a></li>
                  <li><a href="#content_box.html"><i class="#fa fa-check-square-o"></i>Music room</a></li>
                  <li><a href="#icons.html"><i class="fa fa-star"></i>Trampoline</a></li>
                  <li><a href="#progress-bars.html"><i class="fa fa-server"></i> Wall climbing</a></li>
                  <li><a href="#typography.html"><i class="fa fa-link fa-sm"></i>Toy room</a></li>
                </ul>
              </li>
              <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">ABOUT US</a>
                <ul class="dropdown-menu" role="menu">
                  <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Product</a>
                    <ul class="dropdown-menu">
                      <li><a href="http://vrlab.cdcon.org.np/">Paracosma Tv</a></li>
                      <li><a href="http://paracosma.com/">Pashupatinath Tour</a></li>
                    </ul>
                  </li>
                  <li><a href="www.Pracosma.com">Company</a></li>
                </ul>
              </li>





              
              <!--li.dropdown.navbar-cart-->
              <!--    a.dropdown-toggle(href='#', data-toggle='dropdown')-->
              <!--        span.icon-basket-->
              <!--        |-->
              <!--        span.cart-item-number 2-->
              <!--    ul.dropdown-menu.cart-list(role='menu')-->
              <!--        li-->
              <!--            .navbar-cart-item.clearfix-->
              <!--                .navbar-cart-img-->
              <!--                    a(href='#')-->
              <!--                        img(src='assets/images/shop/product-9.jpg', alt='')-->
              <!--                .navbar-cart-title-->
              <!--                    a(href='#') Short striped sweater-->
              <!--                    |-->
              <!--                    span.cart-amount 2 &times; $119.00-->
              <!--                    br-->
              <!--                    |-->
              <!--                    strong.cart-amount $238.00-->
              <!--        li-->
              <!--            .navbar-cart-item.clearfix-->
              <!--                .navbar-cart-img-->
              <!--                    a(href='#')-->
              <!--                        img(src='assets/images/shop/product-10.jpg', alt='')-->
              <!--                .navbar-cart-title-->
              <!--                    a(href='#') Colored jewel rings-->
              <!--                    |-->
              <!--                    span.cart-amount 2 &times; $119.00-->
              <!--                    br-->
              <!--                    |-->
              <!--                    strong.cart-amount $238.00-->
              <!--        li-->
              <!--            .clearfix-->
              <!--                .cart-sub-totle-->
              <!--                    strong Total: $476.00-->
              <!--        li-->
              <!--            .clearfix-->
              <!--                a.btn.btn-block.btn-round.btn-font-w(type='submit') Checkout-->
              <!--li.dropdown-->
              <!--    a.dropdown-toggle(href='#', data-toggle='dropdown') Search-->
              <!--    ul.dropdown-menu(role='menu')-->
              <!--        li-->
              <!--            .dropdown-search-->
              <!--                form(role='form')-->
              <!--                    input.form-control(type='text', placeholder='Search...')-->
              <!--                    |-->
              <!--                    button.search-btn(type='submit')-->
              <!--                        i.fa.fa-search-->
              

              <!-- <li class="dropdown"><a class="dropdown-toggle" href="documentation.html" data-toggle="dropdown">Documentation</a>
                <ul class="dropdown-menu">
                  <li><a href="documentation.html#contact">Contact Form</a></li>
                  <li><a href="documentation.html#reservation">Reservation Form</a></li>
                  <li><a href="documentation.html#mailchimp">Mailchimp</a></li>
                  <li><a href="documentation.html#gmap">Google Map</a></li>
                  <li><a href="documentation.html#plugin">Plugins</a></li>
                  <li><a href="documentation.html#changelog">Changelog</a></li>
                </ul>
              </li> -->
            </ul>
          </div>
        </div>
      </nav>
      <section class="home-section home-full-height" id="home">
        <div class="hero-slider">
          <ul class="slides">
            <li class="bg-dark-30 restaurant-page-header bg-dark" style="background-image:url(&quot;assets/images/restaurant/Screenshot_3.jpg&quot;);">
              <div class="titan-caption">
                <div class="caption-content">
                  <div class="font-alt mb-30 titan-title-size-1"> Hello & welcome to our World</div>
                  <div class="font-alt mb-40 titan-title-size-4">Archery</div><a class="section-scroll btn btn-border-w btn-round" href="#archery">Do some archery and fun here with us</a>
                </div>
              </div>
            </li>
            <li class="bg-dark-30 restaurant-page-header bg-dark" style="background-image:url(&quot;assets/images/restaurant/slider.jpg&quot;);">
            
              <div class="titan-caption">
                <div class="caption-content">
                <div class="font-alt mb-30 titan-title-size-1"> Master bed room for your world</div>
                  <div class="font-alt mb-30 titan-title-size-2"></div><a class="btn btn-border-w btn-round" href="#master_bedroom">Master bedroom for all </a>
                </div>
              </div>
            </li>
            <li class="bg-dark-30 restaurant-page-header bg-dark" style="background-image:url(&quot;assets/images/restaurant/downhallways.jpg&quot;);">
            
              <div class="titan-caption">
                <div class="caption-content">
                  <div class="font-alt mb-30 titan-title-size-1">Down hallway to this royal home </div>
                  <div class="font-alt mb-40 titan-title-size-3">Great hall way to rooms</div><a class="section-scroll btn btn-border-w btn-round" href="#down_hallway_to_hell">Down hallway to way to hall</a>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </section>
      <div class="main">
        <section class="module" id="services">
          <div class="container">
            <div class="row">
              <div class="col-sm-2 col-sm-offset-4">
                <div class="alt-module-subtitle"><span class="holder-w"></span>
                  <h5 class="font-serif">We are now on differnet place for your device </h5>
                  <span class="holder-w"></span>
                </div>
              </div>
            </div>
            <div class="row">
            <hr>
              <div class="col-sm-8 col-sm-offset-2">
                <h2 class="module-title font-alt">OFFICIAL VR Launch title for</h2>
              </div>
            </div>
            <div class="row multi-columns-row ">
              <div class="col-sm-6 col-md-3 col-lg-4 card_hover">
                <div class="features-item">
                  <div class="features-icon"><img src="https://jobsimulatorgame.com/images/platforms/hand_vive.png"></div>
                  <hr>
                  <h3 class="features-title font-alt">HTC Vive</h3>Launch for HTC Vive  , is available <a href="www.paracosma.com"> now on steam store </a>
                </div>
              </div>

              <div class="col-sm-6 col-md-3 col-lg-4 card_hover">
                <div class="features-item">
                  <div class="features-icon"><img src="https://jobsimulatorgame.com/images/platforms/hand_psvr.png"></div>
                  <hr>
                  <h3 class="features-title font-alt">PlayStation VR</h3>Launch for Playstation VR , available  <a href="www.paracosma.com">now on the Playstation store</a>
                </div>
              </div>


              <div class="col-sm-6 col-md-3 col-lg-4 card_hover">
                <div class="features-item">
                  <div class="features-icon"><img src="https://jobsimulatorgame.com/images/platforms/hand_touch.png"></div>
                  <hr>
                  <h3 class="features-title font-alt">Oculus touch</h3>Launch for Oculus Touch , available <a href="www.paracosma.com">now on the oculus store</a>
                </div>
              </div>
              
            </div>
          </div>
        </section>
        <hr class="divider-w">
        <section class="module" id="specialities">
          <div class="container">
            <div class="row">
              <div class="col-sm-2 col-sm-offset-5">
                <div class="alt-module-subtitle"><span class="holder-w"></span>
                  <h5 class="font-serif">Take a look at</h5><span class="holder-w"></span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2">
                <h2 class="module-title font-alt">Our World</h2>
              </div>
            </div>
            <div class="row multi-columns-row">
              <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/r2.jpg" alt="Kabab"/></div>
                  <h3 class="content-box-title font-serif">Girls's Room </h3>
                  <div class="description">
                  This is a room with girl material and make u feel really true that its girl's room and u came without knock.
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/r3.jpg" alt="Limes"/></div>
                   <h3 class="content-box-title font-serif">Master Bed Room</h3>
                   <div class="description">
                   This is a room with Mater and awesome bed and luxorius sofa . Burning fire and dim light is awesome here.
                   </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/r4.jpg" alt="Radish"/></div>
                  <h3 class="content-box-title font-serif">Hall way </h3>
                  <div class="description">
                  This is a Passage to halway that come accross often that make u feel like you are in super awesome home.
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/r5.jpg" alt="Corn"/></div>
                   <h3 class="content-box-title font-serif">Graffety Room </h3>
                   <div class="description">
                   This is a graffety room where you can make awesome design and paint .
                   </div>
                </div>
              </div>
            </div>

            <div class="row multi-columns-row">
              <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/r2.jpg" alt="Kabab"/></div>
                  <h3 class="content-box-title font-serif">Girls's Room </h3>
                  <div class="description">
                  This is a room with girl material and make u feel really true that its girl's room and u came without knock.
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/r3.jpg" alt="Limes"/></div>
                   <h3 class="content-box-title font-serif">Master Bed Room</h3>
                   <div class="description">This is a room with Mater and awesome bed and luxorius sofa . Burning fire and dim light is awesome here.
                   </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/r4.jpg" alt="Radish"/></div>
                  <h3 class="content-box-title font-serif">Hall way </h3>
                  <div class="description">
                  This is a Passage to halway that come accross often that make u feel like you are in super awesome home.
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/r5.jpg" alt="Corn"/></div>
                   <h3 class="content-box-title font-serif">Graffety Room </h3>
                   <div class="description">
                   This is a graffety room where you can make awesome design and paint .
                    </div>
                </div>
              </div>
            </div>


            <div class="row multi-columns-row">
              <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/r2.jpg" alt="Kabab"/></div>
                  <h3 class="content-box-title font-serif">Girls's Room </h3>
                  <div class="description">
                  This is a room with girl material and make u feel really true that its girl's room and u came without knock.
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/r3.jpg" alt="Limes"/></div>
                   <h3 class="content-box-title font-serif">Master Bed Room</h3>This is a room with Mater and awesome bed and luxorius sofa . Burning fire and dim light is awesome here.
                </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/r4.jpg" alt="Radish"/></div>
                  <h3 class="content-box-title font-serif">Hall way </h3>
                  <div class="description">
                  This is a Passage to halway that come accross often that make u feel like you are in super awesome home.
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/r5.jpg" alt="Corn"/></div>
                   <h3 class="content-box-title font-serif">Graffety Room </h3>
                   <div class="description">
                      This is a graffety room where you can make awesome design and paint .
                   </div>
                </div>
              </div>
            </div>


          </div>
        </section>
        <section class="module bg-dark parallax-bg landing-screenshot" data-background="assets/images/restaurant/Screenshot_3.jpg" style="background-image: url("assets/images/landing/Screenshot_3.jpg");">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-sm-offset-3">
                <h2 class="module-title font-alt mb-0">Probably the Best Realistic Game.</h2>
              </div>
            </div>
          </div>
          <div class="video-player" data-property="{videoURL:'https://www.youtube.com/watch?v=i_XV7YCRzKo', containment:'.module-video', startAt:3, mute:true, autoPlay:true, loop:true, opacity:1, showControls:false, showYTLogo:false, vol:25}"></div>
        </section>
        <section class="module" id="menu">
          <div class="container">
            <div class="row">
              <div class="col-sm-2 col-sm-offset-5">
                <div class="alt-module-subtitle"><span class="holder-w"></span>
                  <h5 class="font-serif">About Game</h5><span class="holder-w"></span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2">
                <h2 class="module-title font-alt">Additional detail</h2>
              </div>
            </div>
            <div class="row multi-columns-row">
              <div class="col-sm-6">
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8">
                      <h4 class="menu-title font-alt">Category</h4>
                      <div class="menu-detail font-serif">Games</div>
                    </div>


                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>

                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8">
                      <h4 class="menu-title font-alt">Genre</h4>
                      <div class="menu-detail font-serif">Action / Adventure / Arcade </div>
                    </div>

                   
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>


                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8">
                      <h4 class="menu-title font-alt">Developers</h4>
                      <div class="menu-detail font-serif">Paracosma VR R&D Lab</div>
                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8">
                      <h4 class="menu-title font-alt">Space Required</h4>
                      <div class="menu-detail font-serif">7GB</div>
                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8">
                      <h4 class="menu-title font-alt">Version</h4>
                      <div class="menu-detail font-serif">0.1</div>
                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>

              </div>
              <div class="col-sm-6">
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8">
                      <h4 class="menu-title font-alt">Recommended Graphics Card</h4>
                      <div class="menu-detail font-serif">CardNVIDIA GTX 980 / AMD Radeon R9 290X  or greater
</div>

                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8">
                      <h4 class="menu-title font-alt">Recommended Memory</h4>
                      <div class="menu-detail font-serif">32 GB</div>
                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8">
                      <h4 class="menu-title font-alt">Recommended Processor</h4>
                      <div class="menu-detail font-serif">CPU Intel i5-6600K equivalent or greater</div>
                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8">
                      <h4 class="menu-title font-alt">Supported Controllers</h4>
                      <div class="menu-detail font-serif">VIVE controller , Oculus controller  , Playstation controller </div>
                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8">
                      <h4 class="menu-title font-alt">Language</h4>
                      <div class="menu-detail font-serif">English</div>
                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="module pt-0 pb-0">
          <div class="row position-relative m-0">
            <div class="col-xs-12 col-md-12 col-lg-12 side-image-text">
              <div class="row">
                <div class="col-xs-12 ">
                  <h2 class="module-title font-alt align-left">Awards and Accolades</h2>
                  <p class="module-subtitle font-serif align-left">These are the praises we recieved to our game UNNAMED</p>
                  <div class="image" style="background-color: #f7f7f1;">
                  <hr>
                  <div class="row">
                        <div class="col-sm-2 col-md-2 "></div>
                        <div class="col-sm-3 col-md-3 col-lg-2">
                          <img src="https://jobsimulatorgame.com/images/accolades/GDCA_BestARVR_Game.png">
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-2">
                          <img src="https://jobsimulatorgame.com/images/accolades/unity_awards_2016_Best_VR_Game.png">
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-2">
                          <img src="https://jobsimulatorgame.com/images/accolades/vision_summit_awards_2016_best_vr_experience.png">
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-2">
                          <img src="https://jobsimulatorgame.com/images/accolades/vision_summit_awards_2016_best_vr_experience.png">
                        </div>
                        <div class="col-sm-1 col-md-1"></div>
                  </div>
                  <hr>
                  <div class="row">
                        <div class="col-sm-2 col-md-2 "></div>
                        <div class="col-sm-4 col-md-3 col-lg-2">
                          <img src="https://jobsimulatorgame.com/images/accolades/GDCA_BestARVR_Game.png">
                        </div>
                        <div class="col-sm-4 col-md-3 col-lg-2">
                          <img src="https://jobsimulatorgame.com/images/accolades/unity_awards_2016_Best_VR_Game.png">
                        </div>

                         <div class="col-sm-4 col-md-3 col-lg-2">
                          <img src="https://jobsimulatorgame.com/images/accolades/unity_awards_2016_Best_VR_Game.png">
                        </div>
                         <div class="col-sm-4 col-md-3 col-lg-2">
                          <img src="https://jobsimulatorgame.com/images/accolades/unity_awards_2016_Best_VR_Game.png">
                        </div>
                         <div class="col-sm-1 col-md-1"></div>
                  </div>
                  <hr>
                  <div class="row">
                  <div class="col-sm-2 col-md-2 "></div>
                      <div class="col-sm-4 col-md-3 col-lg-2">
                        <img src="https://jobsimulatorgame.com/images/accolades/unity_awards_2016_Best_VR_Game.png">
                      </div>
                      <div class="col-sm-4 col-md-3 col-lg-2">
                        <img src="https://jobsimulatorgame.com/images/accolades/GDCA_BestARVR_Game.png">
                      </div>
                      <div class="col-sm-4 col-md-3 col-lg-2">
                        <img src="https://jobsimulatorgame.com/images/accolades/unity_awards_2016_Best_VR_Game.png">
                      </div>
                      <div class="col-sm-4 col-md-3 col-lg-2">
                        <img src="https://jobsimulatorgame.com/images/accolades/unity_awards_2016_Best_VR_Game.png">
                      </div>
                       <div class="col-sm-1 col-md-1"></div>
                </div>
                <hr>
                </div>
              </div>
            </div>
          </div>
        </section>


      <section class="module module_2 pt-0 pb-0">
          <div class="row position-relative m-0">
         
            <div class="col-sm-offset-2 col-sm-8">
              <iframe width=100% height="480" src="https://www.youtube.com/embed/Kdaoe4hbMso?autoplay=1&loop=1" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>
        </section>

<hr>
    <section class="module module_2 " id="awan"> 
<div class="container">
            <div class="intro-header"> 
                    <div class="container"  align="center">

                    <div class="tab-content custom-tab-content" align="center">
                    <div class="subscribe-panel">
                    <h1>Newsletter!!!</h1>
                    <p>Subscribe to our weekly Newsletter and stay tuned.</p>
                                    
                                 <form action="" method="post" >
                                          
                                  <div class="col-md-4"></div>
                                  <div class="col-md-4">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i></span>
                                      <input type="text" class="form-control input-lg" name="email" id="email"  placeholder="Enter your Email" autocomplete="off"/>
                                    </div>
                                  </div>
                                  <div class="col-md-4"></div>
                                        <br/><br/><br/>
                                        <button class="btn btn-warning btn-lg">Subscribe Now!</button>
                                  </form>

                    </div>
                    </div>
                    </div>
      </div>
            <div class=""></div>



    <div class="col-lg-offset-4 col-lg-4" id="panel">
        <h2>Message Us</h2>

        <form id="contact-form" class="contact-form" action="{{url('send')}}">

            <div class="group">
                <input type="text" name="name" autocomplete="off" id="ame"  required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Name</label>
            </div>

            <div class="group">
                <input type="text"  name="email" autocomplete="off" id="email" required >
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Email</label>
            </div>

            <div class="group">
                <input type="text" name="number" autocomplete="off" id="mobile" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Mobile No</label>
            </div>

            <div class="group">
                <input type="text"  name="Message" autocomplete="off" id="Message" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Message</label>
            </div>
            <div class="group">
                <center> <button type="submit" class="btn btn-warning">Send <span class="glyphicon glyphicon-send"></span></button></center>
            </div>
        </form>

    </div>
</div>


    </section>
     
        <footer class="footer bg-dark">
          <div class="container">
            <div class="row">
              <div class="col-sm-6">
                <p class="copyright font-alt">&copy; 2017&nbsp;<a href="http://paracosma.com/">PARACOSMA</a>, All Rights Reserved</p>
              </div>
              <div class="col-sm-6">
                <div class="footer-social-links"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-dribbble"></i></a><a href="#"><i class="fa fa-skype"></i></a>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
      <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
    </main>
    <!--  
    JavaScripts
    =============================================
    -->
    <script src="{{ asset('assets/lib/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('assets/lib/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/lib/wow/dist/wow.js') }}"></script>
    <script src="{{ asset('assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js') }}"></script>
    <script src="{{ asset('assets/lib/isotope/dist/isotope.pkgd.js') }}"></script>
    <script src="{{ asset('assets/lib/imagesloaded/imagesloaded.pkgd.js') }}"></script>
    <script src="{{ asset('assets/lib/flexslider/jquery.flexslider.js') }}"></script>
    <script src="{{ asset('assets/lib/owl.carousel/dist/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/lib/smoothscroll.js') }}"></script>
    <script src="{{ asset('assets/lib/magnific-popup/dist/jquery.magnific-popup.js') }}"></script>
    <script src="{{ asset('assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js') }}"></script>
    <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>
    <script src="{{ asset('assets/js/plugins.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
  </body>
</html>