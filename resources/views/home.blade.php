<!DOCTYPE html>
<html lang="en-US" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--  
    Document Title
    =============================================
    -->
    <title>UNNAMED | VR Game</title>
    <!--  
    Favicons
    =============================================
    -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/images/favicons/apple-icon-57x57.png') }}" ">
<!--     <link rel="apple-touch-icon" sizes="60x60" href="assets/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
 -->    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/images/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--  
    Stylesheets
    =============================================
    
    -->
    <!-- Default stylesheets-->
    <link href= "{{ asset('assets/lib/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Template specific stylesheets-->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="{{ asset('assets/lib/animate.css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/components-font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/et-line-font/et-line-font.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/flexslider/flexslider.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/owl.carousel/dist/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/owl.carousel/dist/assets/owl.theme.default.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/magnific-popup/dist/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/simple-text-rotator/simpletextrotator.css') }}" rel="stylesheet">
    <!-- Main stylesheet and color file-->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link id="color-scheme" href="assets/css/colors/default.css') }}" rel="stylesheet">
  </head>
  <style type="text/css">
    .module_4{
padding: 0px 0px 10px 0px;
}

  </style>
  <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
    <main>
      <div class="page-loader">
        <div class="loader">Loading...</div>
      </div>
      <section>
      <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <a class="navbar-brand" href="{{url('/') }}">UNNAMED</a>
          </div>
          <div class="collapse navbar-collapse" id="custom-collapse">
           <!--  <ul class="nav navbar-nav navbar-right2">
              <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Features</a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#alerts-and-wells.html"><i class="fa fa-bolt"></i>Archery</a></li>
                  <li><a href="#buttons.html"><i class="fa fa-link fa-sm"></i>Graffiti </a></li>
                  <li><a href="#tabs_and_accordions.html"><i class="fa fa-tasks"></i>Ping pong &amp; Pool</a></li>
                  <li><a href="#content_box.html"><i class="#fa fa-check-square-o"></i>Music room</a></li>
                  <li><a href="#icons.html"><i class="fa fa-star"></i>Trampoline</a></li>
                  <li><a href="#progress-bars.html"><i class="fa fa-server"></i> Wall climbing</a></li>
                  <li><a href="#typography.html"><i class="fa fa-link fa-sm"></i>Toy room</a></li>
                </ul>
              </li>
              <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">ABOUT US</a>
                <ul class="dropdown-menu" role="menu">
                  <li class="dropdown-submenu pull-left"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Product</a>
                    <ul class="dropdown-menu" style="float: right;">
                      <li><a href="http://vrlab.cdcon.org.np/">Paracosma Tv</a></li>
                      <li><a href="http://paracosma.com/">Pashupatinath Tour</a></li>
                    </ul>
                  </li>
                  <li><a href="https://www.Pracosma.com">Company</a></li>
                </ul>
              </li>
              </ul> -->








              
              <!--li.dropdown.navbar-cart-->
              <!--    a.dropdown-toggle(href='#', data-toggle='dropdown')-->
              <!--        span.icon-basket-->
              <!--        |-->
              <!--        span.cart-item-number 2-->
              <!--    ul.dropdown-menu.cart-list(role='menu')-->
              <!--        li-->
              <!--            .navbar-cart-item.clearfix-->
              <!--                .navbar-cart-img-->
              <!--                    a(href='#')-->
              <!--                        img(src='assets/images/shop/product-9.jpg', alt='')-->
              <!--                .navbar-cart-title-->
              <!--                    a(href='#') Short striped sweater-->
              <!--                    |-->
              <!--                    span.cart-amount 2 &times; $119.00-->
              <!--                    br-->
              <!--                    |-->
              <!--                    strong.cart-amount $238.00-->
              <!--        li-->
              <!--            .navbar-cart-item.clearfix-->
              <!--                .navbar-cart-img-->
              <!--                    a(href='#')-->
              <!--                        img(src='assets/images/shop/product-10.jpg', alt='')-->
              <!--                .navbar-cart-title-->
              <!--                    a(href='#') Colored jewel rings-->
              <!--                    |-->
              <!--                    span.cart-amount 2 &times; $119.00-->
              <!--                    br-->
              <!--                    |-->
              <!--                    strong.cart-amount $238.00-->
              <!--        li-->
              <!--            .clearfix-->
              <!--                .cart-sub-totle-->
              <!--                    strong Total: $476.00-->
              <!--        li-->
              <!--            .clearfix-->
              <!--                a.btn.btn-block.btn-round.btn-font-w(type='submit') Checkout-->
              <!--li.dropdown-->
              <!--    a.dropdown-toggle(href='#', data-toggle='dropdown') Search-->
              <!--    ul.dropdown-menu(role='menu')-->
              <!--        li-->
              <!--            .dropdown-search-->
              <!--                form(role='form')-->
              <!--                    input.form-control(type='text', placeholder='Search...')-->
              <!--                    |-->
              <!--                    button.search-btn(type='submit')-->
              <!--                        i.fa.fa-search-->
              

              <!-- <li class="dropdown"><a class="dropdown-toggle" href="documentation.html" data-toggle="dropdown">Documentation</a>
                <ul class="dropdown-menu">
                  <li><a href="documentation.html#contact">Contact Form</a></li>
                  <li><a href="documentation.html#reservation">Reservation Form</a></li>
                  <li><a href="documentation.html#mailchimp">Mailchimp</a></li>
                  <li><a href="documentation.html#gmap">Google Map</a></li>
                  <li><a href="documentation.html#plugin">Plugins</a></li>
                  <li><a href="documentation.html#changelog">Changelog</a></li>
                </ul>
              </li> -->
            
          </div>
        </div>
      </nav>
      </section>

      <section class="notification-sec">
        
             @if(Session::has('status_done'))
               <div class="content alert alert-dismissable">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-2 col-lg-5"></div>
                  <div class="col-md-6 col-sm-6 col-xs-8 col-lg-2 ribbon_div">
                  <button type="button" class="btn btn-warning ribbon">Thanks</button>
                  </div>
                <div class="col-md-3 col-sm-3 col-xs-2 col-lg-5"></div>
                </div>
                <div class="col-md-1 col-sm-1 col-lg-3"></div>
                  <div class="col-md-10 col-sm-10 col-xs-12 col-lg-6">
                  <div class=" alert-success ">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  Thanks for Subscribing UNNAMED VR !!! 
                </div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-12 col-lg-6"></div>

              </div>

          @elseif(Session::has('status_error'))
                <div class="content alert alert-dismissable">
                  <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-2 col-lg-5"></div>
                  <div class="col-md-6 col-sm-6 col-xs-8 col-lg-2 ribbon_div">
                  <button type="button" class="btn btn-danger ribbon">Sorry</button>
                  </div>
                <div class="col-md-3 col-sm-3 col-xs-2 col-lg-5"></div>
                </div>
                  <div class="col-md-1 col-sm-1 col-lg-3"></div>
                  <div class="col-md-10 col-sm-10 col-xs-12 col-lg-6">
                    <div class=" alert-danger danger ">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="color: white;">×</button>
                    Email is already in use .. please try other email address !!!
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-lg-3"></div>
                  </div>
 

          @elseif(Session::has('mail_success'))
                <div class="content alert alert-dismissable">
                   <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-2 col-lg-5"></div>
                  <div class="col-md-6 col-sm-6 col-xs-8 col-lg-2 ribbon_div">
                  <button type="button" class="btn btn-warning ribbon">Thank you</button>
                  </div>
                <div class="col-md-3 col-sm-3 col-xs-2 col-lg-5"></div>
                </div>
                 <div class="col-md-1 col-sm-1 col-lg-3"></div>
                  <div class="col-md-10 col-sm-10 col-xs-12 col-lg-6">
                 <div class=" alert-success ">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                   Thanks for Mail . we will reply soon  !!!
               </div>
                </div>
                <div class="col-md-1 col-sm-1 col-lg-3"></div>
                </div>
          @endif
      </section>
      
      <section class="home-section home-full-height" id="home">
        
        <div class="hero-slider">
          <ul class="slides">

            <li class="bg-dark-30 restaurant-page-header bg-dark" style="background-image:url(&quot;assets/images/restaurant/Screenshot_3.jpg&quot;);">
              <div class="titan-caption">
                <div class="caption-content">
                  <div class="font-alt mb-30 titan-title-size-1">welcome to our World</div>
                  <div class="font-alt mb-40 titan-title-size-4">Archery</div><a class="section-scroll btn btn-border-w btn-round" href="#archery">Do some archery </a>
                </div>
              </div>

            </li>

            <li class="bg-dark-30 restaurant-page-header bg-dark" style="background-image:url(&quot;assets/images/restaurant/slider.jpg&quot;);">
            
              <div class="titan-caption">
                <div class="caption-content">
                <div class="font-alt mb-30 titan-title-size-1"> Master bed room </div>
                  <div class="font-alt mb-30 titan-title-size-2"></div><a class="btn btn-border-w btn-round" href="#master_bedroom">Master bedroom  </a>
                </div>
              </div>
            </li>
            <li class="bg-dark-30 restaurant-page-header bg-dark" style="background-image:url(&quot;assets/images/restaurant/downhallways.jpg&quot;);">
            
              <div class="titan-caption">
                <div class="caption-content">
                  <div class="font-alt mb-30 titan-title-size-1">Down hallway to home </div>
                  <div class="font-alt mb-40 titan-title-size-3">Great hall to rooms</div><a class="section-scroll btn btn-border-w btn-round" href="#down_hallway_to_hell">Down hallway to hall</a>

                  
                </div>

              </div>

              
            </li>
          </ul>


        </div>

        <div class="buy_now">
          <ul style="list-style-type: none;">
              <li><button class="btn btn-warning btn_buy"> <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                  BUY IT NOW ON STEAM</button></li>
              <li><button class="btn btn-warning btn_buy"> <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                  BUY IT NOW ON VIVE</button></li>
              <li><button class="btn btn-warning btn_buy"> <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                  BUY IT NOW ON PLAYSTATION</button></li>
          </ul>
        </div>


      </section>
      <div class="main">
        <div class="releasing">
            <link href='https://fonts.googleapis.com/css?family=Montserrat:700|Amatic+SC' rel='stylesheet' type='text/css'>
                  <div align="center" style="margin-top:120px;">
                        <div class="clip-text clip-text_one">Releasing soon!!!</div>
                  </div>
        </div>

        <div class="releasing_moz" style="display: none;">
         <div align="center" style="margin-top:120px;">
            <button class="btn  release_btn btn_buy2">RELEASING SOON !!!</button>
        </div>
        </div>
        

        <section class="module" id="services">
          <div class="container">
            <div class="row">
              <div class="col-sm-2 col-sm-offset-4 col-xs-12">
                <div class="alt-module-subtitle"><span class="holder-w"></span>
                  <h5 class="font-serif">We are now on differnet place for your device </h5>
                  <span class="holder-w"></span>
                </div>
              </div>
            </div>
            <div class="row">
            <hr>
              <div class="col-sm-8 col-sm-offset-2 col-xs-12">
                <h2 class="module-title font-alt">OFFICIAL VR Launch title for</h2>
              </div>
            </div>
            <div class="row multi-columns-row ">
              <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 card_hover">
                <div class="features-item">
                  <div class="features-icon"><img src="https://jobsimulatorgame.com/images/platforms/hand_vive.png"></div>
                  <hr>
                  <h3 class="features-title font-alt">HTC Vive</h3>Launch for HTC Vive  , is available <a href="http://paracosma.com/"> now on steam store </a>
                </div>
              </div>

              <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 card_hover">
                <div class="features-item">
                  <div class="features-icon"><img src="https://jobsimulatorgame.com/images/platforms/hand_psvr.png"></div>
                  <hr>
                  <h3 class="features-title font-alt">PlayStation VR</h3>Launch for Playstation VR , available  <a href="http://paracosma.com/">now on the Playstation store</a>
                </div>
              </div>


              <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 card_hover">
                <div class="features-item">
                  <div class="features-icon"><img src="https://jobsimulatorgame.com/images/platforms/hand_touch.png"></div>
                  <hr>
                  <h3 class="features-title font-alt">Oculus touch</h3>Launch for Oculus Touch , available <a href="http://paracosma.com/">now on the oculus store</a>
                </div>
              </div>
              
            </div>
          </div>
        </section>
        <hr class="divider-w">
        <section class="module module_4" id="specialities">
         <div class="content">
         <div id="">
           <div class="col-1-1">
             <h1 class="padleft">First floor room</h1>
          </div>
        
          
          <div class="col-1-1">
          <div class="module module_4 magnify">
              <div class="large"></div>
              <img width="100%" src="assets/images/restaurant/hallways_top.jpg" class="cardimg2 small" id="smith">
              <div class="cardoverlay"><span style="font-size: 12px; color: white;">Hallway of First floor</span>
              </div>
            </div>
          </div>
          
        </div><!--end of wrapper-->
        </div>



          <div class="container">
            <div class="row">
              <div class="col-sm-2 col-sm-offset-5 col-xs-12">
                <div class="alt-module-subtitle"><span class="holder-w"></span>
                  <h5 class="font-serif">Take a look at</h5><span class="holder-w"></span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2 col-xs-12">
                <h2 class="module-title font-alt">Our World</h2>
              </div>
            </div>
            <div class="row multi-columns-row">
              <div class="col-sm-6 col-md-4 col-lg-3 col-xs-12">
                <div class="content-box card_hover ">
                  <div class="content-box-image"><img src="assets/images/restaurant/girls-room.png" alt="Kabab"/></div>
                  <h3 class="content-box-title font-serif">Girls's Room </h3>
                  <div class="description" id="r1">
                  This is a room with girl material and make u feel really true that its girl's room and u came without knock.
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4 col-lg-3 col-xs-12">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/climbing-room.png" alt="Limes"/></div>
                   <h3 class="content-box-title font-serif">Adventure Room</h3>
                   <div class="description" id="r2">
                   This is a room with Mater and awesome bed and luxorius sofa . Burning fire and dim light is awesome here.
                   </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4 col-lg-3 col-xs-12">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/tt.png" alt="Radish"/></div>
                  <h3 class="content-box-title font-serif">Pingpong Room</h3>
                  <div class="description" id="r3">
                  This is a Passage to halway that come accross often that make u feel like you are in super awesome home.
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4 col-lg-3 col-xs-12">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/boyroom.png" alt="Corn"/></div>
                   <h3 class="content-box-title font-serif">Boy's Room </h3>
                   <div class="description" id="r4">
                   This is a graffety room where you can make awesome design and paint .
                   </div>
                </div>
              </div>

              <div class="col-sm-6 col-md-4 col-lg-3 col-md-offset-4 col-lg-offset-4 col-xs-12">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/guest-room.png" alt="Kabab"/></div>
                  <h3 class="content-box-title font-serif">Store Room</h3>
                  <div class="description" id="r9">
                  This is a room with girl material and make u feel really true that its girl's room and u came without knock.
                  </div>
                </div>
              </div>
            </div>
            </div>


          <div class="content">
         <div id="colwrapper">
           <div class="col-1-1">
             <h1 class="padleft">Second Floor </h1>
          </div>
        
          
          <div class="col-1-1">
            <div class="module module_3 magnifyb">
              <div class="largeb"></div>
              <img width="100%" src="assets/images/restaurant/hallways_top.jpg" class="cardimg2 smallb" id="smith">
              <div class="cardoverlay"><span style="font-size: 12px; color: white;">Hallway of Second floor</span>
              </div>
            </div>
          </div>


          <div class="">
            <div class="row">
              <div class="col-sm-2 col-sm-offset-5">
                <div class="alt-module-subtitle"><span class="holder-w"></span>
                  <h5 class="font-serif">Take a look at</h5><span class="holder-w"></span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2 col-xs-12">
                <h2 class="module-title font-alt">Our World</h2>
              </div>
            </div>
                <div class="row multi-columns-row">
              <div class="col-sm-6 col-md-3 col-lg-3 col-xs-12">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/paint-room.png" alt="Kabab"/></div>
                  <h3 class="content-box-title font-serif">Grafity Room</h3>
                  <div class="description" id="r5">
                  This is a room with girl material and make u feel really true that its girl's room and u came without knock.
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3 col-xs-12">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/Screenshot_3.png" alt="Limes"/></div>
                   <h3 class="content-box-title font-serif">Archery room</h3>
                   <div class="description" class="r6">
                    This is a room with Mater and awesome bed and luxorius sofa . Burning fire and dim light is awesome here.
                   </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3 col-xs-12">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/slide.png" alt="Radish"/></div>
                  <h3 class="content-box-title font-serif">Mater Bedroom </h3>
                  <div class="description" id="r7">
                  This is a Passage to halway that come accross often that make u feel like you are in super awesome home.
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3 col-xs-12">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/store.png" alt="Corn"/></div>
                   <h3 class="content-box-title font-serif">Store Room </h3>
                   <div class="description" id="r8">
                   This is a graffety room where you can make awesome design and paint .
                    </div>
                </div>
              </div>
            </div>


            <div class="row multi-columns-row">
              <div class="col-sm-6 col-md-3 col-lg-3 col-xs-12">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/block.png" alt="Kabab"/></div>
                  <h3 class="content-box-title font-serif">Block Room</h3>
                  <div class="description" id="r9">
                  This is a room with girl material and make u feel really true that its girl's room and u came without knock.
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3 col-xs-12">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/kitchen.png" alt="Limes"/></div>
                   <h3 class="content-box-title font-serif">Kitchen</h3>
                   <div class="description" id="r10">
                   This is a room with Mater and awesome bed and luxorius sofa . Burning fire and dim light is awesome here.
                </div>
              </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3 col-xs-12">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/music.png" alt="Radish"/></div>
                  <h3 class="content-box-title font-serif">Music room</h3>
                  <div class="description" id="r11">
                  This is a Passage to halway that come accross often that make u feel like you are in super awesome home.
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3 col-xs-12">
                <div class="content-box card_hover">
                  <div class="content-box-image"><img src="assets/images/restaurant/r4.jpg" alt="Corn"/></div>
                   <h3 class="content-box-title font-serif">Bathroom</h3>
                   <div class="description" id="r12">
                      
                      This is a graffety room where you can make awesome design and paint .
                   
                   </div>
                </div>
              </div>
            </div>
          
        </div><!--end of wrapper-->
        </div>






          </div>
        </section>

       
        <section class="module module_2 bg-dark parallax-bg landing-screenshot" data-background="assets/images/restaurant/Screenshot_3.jpg">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-sm-offset-3 col-xs-12">
                <h2 class="module-title font-alt mb-0">Probably the Best Realistic Game.</h2>
              </div>
            </div>
          </div>
          <div class="video-player" data-property="{videoURL:'https://www.youtube.com/watch?v=i_XV7YCRzKo', containment:'.module-video', startAt:3, mute:true, autoPlay:true, loop:true, opacity:1, showControls:false, showYTLogo:false, vol:25}"></div>
        </section>
        <section class="module" id="menu">
          <div class="container">
            <div class="row">
              <div class="col-sm-2 col-sm-offset-5 col-xs-12">
                <div class="alt-module-subtitle"><span class="holder-w"></span>
                  <h5 class="font-serif">About Game</h5><span class="holder-w"></span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2 col-xs-12">
                <h2 class="module-title font-alt">Additional detail</h2>
              </div>
            </div>
            <div class="row multi-columns-row">
              <div class="col-sm-6 col-xs-12">
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8 col-xs-12">
                      <h4 class="menu-title font-alt">Category</h4>
                      <div class="menu-detail font-serif">Games</div>
                    </div>


                    <div class="col-sm-4 menu-price-detail ">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>

                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8 col-xs-12">
                      <h4 class="menu-title font-alt">Genre</h4>
                      <div class="menu-detail font-serif">Action / Adventure / Arcade </div>
                    </div>

                   
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>


                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8 col-xs-12">
                      <h4 class="menu-title font-alt">Developers</h4>
                      <div class="menu-detail font-serif">Paracosma VR R&D Lab</div>
                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8 col-xs-12">
                      <h4 class="menu-title font-alt">Minimum Storage Required </h4>
                      <div class="menu-detail font-serif">7GB</div>
                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8 col-xs-12">
                      <h4 class="menu-title font-alt">Version</h4>
                      <div class="menu-detail font-serif">0.1</div>
                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>

              </div>
              <div class="col-sm-6 col-xs-12">
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8 col-xs-12">
                      <h4 class="menu-title font-alt"> Recommended Graphics </h4>
                      <div class="menu-detail font-serif">NVIDIA GTX 980  or greater
</div>

                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8 col-xs-12">
                      <h4 class="menu-title font-alt">Supported Controllers</h4>
                      <div class="menu-detail font-serif">VIVE , Oculus  , Playstation controller </div>
                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8 col-xs-12">
                      <h4 class="menu-title font-alt">Recommended Processor</h4>
                      <div class="menu-detail font-serif">CPU Intel i5 equivalent or greater</div>
                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8 col-xs-12">
                      <h4 class="menu-title font-alt">Recommended Memory</h4>
                      <div class="menu-detail font-serif">32 GB</div>
                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>
                <div class="menu">
                  <div class="row">
                    <div class="col-sm-8 col-xs-12">
                      <h4 class="menu-title font-alt">Language</h4>
                      <div class="menu-detail font-serif">English</div>
                    </div>
                    <div class="col-sm-4 menu-price-detail">
                      <h4 class="menu-price font-alt"></h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="module pt-0 pb-0">
          <div class="row position-relative m-0">
            <div class="col-xs-12 col-md-12 col-lg-12 side-image-text">
              <div class="row">
                <div class="col-xs-12">
                  <h2 class="module-title font-alt align-left">Awards and Accolades</h2>
                  <p class="module-subtitle font-serif align-left">These are the praises we recieved to our game UNNAMED</p>
                  <div class="image" style="background-color: #f7f7f1;">
                  <hr>
                  <div class=row">
                        
                         <div class="col-sm-3 col-md-3 col-lg-2 col-xs-9 col-xs-offset-1">
                          <img src="https://jobsimulatorgame.com/images/accolades/vision_summit_awards_2016_best_vr_experience.png">
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-2 col-xs-9 col-xs-offset-1">
                          <img src="https://jobsimulatorgame.com/images/accolades/vision_summit_awards_2016_best_vr_experience.png">
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-2 col-xs-9 col-xs-offset-1">
                          <img src="https://jobsimulatorgame.com/images/accolades/vision_summit_awards_2016_best_vr_experience.png">
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-2 col-xs-9 col-xs-offset-1">
                          <img src="https://jobsimulatorgame.com/images/accolades/vision_summit_awards_2016_best_vr_experience.png">
                        </div>
                        
                        <div class="col-sm-3 col-md-3 col-lg-2 col-xs-9 col-xs-offset-1">
                          <img src="https://jobsimulatorgame.com/images/accolades/GDCA_BestARVR_Game.png">
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-2 col-xs-9 col-xs-offset-1">
                          <img src="https://jobsimulatorgame.com/images/accolades/unity_awards_2016_Best_VR_Game.png">
                        </div>

                         <div class="col-sm-3 col-md-3 col-lg-2 col-xs-9 col-xs-offset-1">
                          <img src="https://jobsimulatorgame.com/images/accolades/unity_awards_2016_Best_VR_Game.png">
                        </div>
                         <div class="col-sm-3 col-md-3 col-lg-2 col-xs-9 col-xs-offset-1">
                          <img src="https://jobsimulatorgame.com/images/accolades/unity_awards_2016_Best_VR_Game.png">
                        </div>
                         
                  </div>
                  
                  <!-- <div class="row">
                  <div class="col-sm-2 col-md-2 "></div>
                      <div class="col-sm-4 col-md-3 col-lg-2">
                        <img src="https://jobsimulatorgame.com/images/accolades/unity_awards_2016_Best_VR_Game.png">
                      </div>
                      <div class="col-sm-4 col-md-3 col-lg-2">
                        <img src="https://jobsimulatorgame.com/images/accolades/GDCA_BestARVR_Game.png">
                      </div>
                      <div class="col-sm-4 col-md-3 col-lg-2">
                        <img src="https://jobsimulatorgame.com/images/accolades/unity_awards_2016_Best_VR_Game.png">
                      </div>
                      <div class="col-sm-4 col-md-3 col-lg-2">
                        <img src="https://jobsimulatorgame.com/images/accolades/unity_awards_2016_Best_VR_Game.png">
                      </div>
                       <div class="col-sm-1 col-md-1"></div>
                </div> -->
               
                </div>
              </div>
            </div>
          </div>
        </section>


      <section class="module module_2 pt-0 pb-0">
          <div class="row position-relative m-0">
            <div class="col-sm-offset-2 col-sm-8 col-xs-12 trailer" data-src="background: url('http://maxpixel.freegreatpicture.com/static/photo/1x/Sustainability-Pointer-Wait-Time-Clock-Symbol-2105690.jpg') ;">
              <iframe class="delay" width=100% height="480" data-src="https://www.youtube.com/embed/C1ZmlK-V0pE?enablejsapi=1&amp;version=3&amp;rel=0&amp;autoplay=1&amp;showsearch=0&amp;autohide=1&amp;border=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>
        </section>

<hr>
    <section class="module module_2 " id="awan"> 
<div class="container">
            <div class="intro-header"> 
                    <div class=""  align="center">

                    <div class="tab-content custom-tab-content" align="center">
                    <div class="subscribe-panel">
                    <h1>Newsletter !!!</h1>
                    <p>Subscribe to our weekly Newsletter and stay tuned.</p>          
                            <form action="{{ url('subscribe')}}" class="contact-form" id="contact_form_subs" method="get" value="{{ csrf_token() }}">
                               <!--    <button type=submit onclick="return false;" style="display:none;"></button>  --> 
                               <fieldset>     
                                  
                                  <div class="col-lg-offset-4 col-lg-4 col-md-7 col-md-offset-3 col-md-6 col-sm-8 col-sm-offset-2 col-xs-12">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class=" glyphicon glyphicon-envelope" aria-hidden="true"></i></span>

                                      <input type="email"  name="email_subs" class="form-control input-lg" autocomplete="off" id="email_subs"   required >

                                      
                                    </div>
                                  </div>
                                    

                                    <div class="col-md-5 col-sm-6  col-sm-offset-3 col-xs-12">
                                      <div class="gap"></div>
                                        <button type="submit" class="btn btn-warning btn-lg" >Subscribe Now! <i class="fa fa-plus-square" aria-hidden="true"></i>
</button>
                                    </div>
                             </fieldset>
                            </form>

                    </div>
                    </div>
                    </div>
      </div>
            <div class=""></div>


            <div class="row">
    <div class="col-lg-offset-4 col-lg-4 col-md-7 col-md-offset-3 col-md-6 col-sm-8 col-sm-offset-2 col-xs-12" id="panel">
      <div class="gap-high"></div>
          <div style="text-align: center;">
         <img src="http://www.officialpsds.com/images/thumbs/Spiderman-Logo-psd59240.png" alt="Logo" title="Logo" width="138">
       </div>
         <h1 class="display1">Message us</h1>
         <div class="gap-high"></div>
        <form id="contact_form" class="contact-form" method="get" action="{{url('send')}}" >
          <fieldset>
            <div class="form-group">
                <input type="text" name="name" autocomplete="off" id="name" required >
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Name</label>
            </div>

            <div class="gap"></div>
            <div class="gap"></div>
            <div class="form-group">
                <input type="email"  name="email" autocomplete="off" id="email"  required >
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Email</label>
            </div>

            <div class="gap"></div>
            <div class="gap"></div>
            <div class="form-group">
                <input type="text"  name="Message" autocomplete="off" id="Message" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Message</label>
            </div>
            
              <div class="gap-high"></div>
                 <center><button type="submit" class="btn btn-warning btn-lg">Send <i class="fa fa-paper-plane" aria-hidden="true"></i>
</button></center>
              <div class="gap"></div>
              <div class="gap"></div>
            </fieldset>
           
        </form>

    </div>
  </div>
</div>


    </section>
     
        <footer class="footer bg-dark">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 co-md-5 col-xs-12 pad1">
                <p class="copyright font-alt">&copy; 2017&nbsp;<a href="http://paracosma.com/">PARACOSMA</a>, All Rights Reserved</p>
              </div>

                <div class="col-sm-3 col-md-2 col-xs-12 menu_last">
                  
                    <div class="dropup pad" style="cursor: pointer;">
                        <span class="copyright font-alt dropdown-toggle" type="button" data-toggle="dropdown">OTHER PRODUCTS<span class="caret"></span></span>
                        <ul class="dropdown-menu" >
                          <li style="padding:12px;width: 100%;" class="border_buttom"><a href="http://vrlab.cdcon.org.np/">PARACOSMA TV</a></li>
                          
                          <li style="padding:12px;"><a href="http://paracosma.com/">PASHUPATI TEMPLE</a></li>
                        </ul>
                    </div>
                  </li>
                </div>
              <div class="col-sm-offset-1 col-sm-2 col-md-offset-1 col-md-2 col-xs-12 pad1">
                <div class="footer-social-links"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-skype"></i></a>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
      <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
    </main>
    <!--  
    JavaScripts
    =============================================
    -->
   
    <!-- <script src="{{ asset('assets/lib/jquery/dist/jquery.js') }}"></script> -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/lib/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/lib/wow/dist/wow.js') }}"></script>
    <script src="{{ asset('assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js') }}"></script>
    <script src="{{ asset('assets/lib/isotope/dist/isotope.pkgd.js') }}"></script>
    <script src="{{ asset('assets/lib/imagesloaded/imagesloaded.pkgd.js') }}"></script>
    <script src="{{ asset('assets/lib/flexslider/jquery.flexslider.js') }}"></script>
    <script src="{{ asset('assets/lib/owl.carousel/dist/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/lib/smoothscroll.js') }}"></script>
    <script src="{{ asset('assets/lib/magnific-popup/dist/jquery.magnific-popup.js') }}"></script>
    <script src="{{ asset('assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>

     <!-- <script type="text/javascript">
      
      $('#BtnNext').hide();
      $("#form1").bind("keypress", function (e) {
            if (e.keyCode == 13) {
                return false;
            }
        });
      function validateEmailDomain(str) {
        var legalDomains ={
        "-yahoo.com" :true,
        "-gmail.com":true,
        "-hotmail.com":true,
        "-msn.com":true,
        "-outlook.com":true,
        "-inbox.com":true,
        "-icloud.com":true
      };

        var matches = str.match(/@(.*)$/);
          if(matches){
            console.log("hello world");
            if(("-" + matches[1]) in legalDomains){

                 $('#BtnNext').show();
                 return(true);
              
            
          }
          return(false);
        }


        $("#emaila").keyup(function() {
           
            $("#result").html(validateEmailDomain(this.value) ? '<i class="fa  fa-check success" aria-hidden="true"></i>':'<i class="fa fa-close danger" aria-hidden="true"></i>');
        });

    </script> -->

    <script type="text/javascript">
      
  //       $(document).ready(function() {
  //   $('#contact_form').bootstrapValidator({});
  // });




          $(document).ready(function() {
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                        stringLength: {
                        min: 3,
                    },
                        notEmpty: {
                        message: 'Please supply your first name (min 3 letter)'
                    }
                }
            },
            
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your email address'
                    },
                    emailAddress: {
                        message: 'Please supply a valid email address'
                    }
                }
            },

            Message: {
                validators: {
                     stringLength: {
                        min: 6,
                    },
                    notEmpty: {
                        message: 'Please supply your message (must be atleast 6 letter)'
                    }
                }
            }
            
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
  




        $('#contact_form_subs').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
          fields: {
              
              email_subs: {
                  validators: {
                      notEmpty: {
                          message: 'Please supply your email address'
                      },
                      emailAddress: {
                          message: 'Please supply a valid email address'
                      }
                  }
              }
              }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form_subs').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });


});



          $(document).ready(function(){

            var native_width = 0;
            var native_height = 0;
            $(".large").css("background","url('" + $(".small").attr("src") + "') no-repeat");

            //Now the mousemove function
            $(".magnify").mousemove(function(e){
              if(!native_width && !native_height)
              {
                var image_object = new Image();
                image_object.src = $(".small").attr("src");
                
                native_width = image_object.width;
                native_height = image_object.height;
                console.log(native_width , native_width);
              }
              else
              {
                var magnify_offset = $(this).offset();
                var mx = e.pageX - magnify_offset.left;
                var my = e.pageY - magnify_offset.top;
                
                if(mx < $(this).width() && my < $(this).height() && mx > 0 && my > 0)
                {
                  $(".large").fadeIn(100);
                }
                else
                {
                  $(".large").fadeOut(100);
                }
                if($(".large").is(":visible"))
                {
                 
                  var rx = Math.round(mx/$(".small").width()*native_width - $(".large").width()/2)*-1;
                  var ry = Math.round(my/$(".small").height()*native_height - $(".large").height()/2)*-1;
                  var bgp = rx + "px " + ry + "px";
                  
                  var px = mx - $(".large").width()/2;
                  var py = my - $(".large").height()/2;
                  $(".large").css({left: px, top: py, backgroundPosition: bgp});
                }
              }
            })
});


  $(document).ready(function(){

              var native_width = 0;
              var native_height = 0;
              $(".largeb").css("background","url('" + $(".smallb").attr("src") + "') no-repeat");

              //Now the mousemove function
              $(".magnifyb").mousemove(function(e){
                if(!native_width && !native_height)
                {
                  var image_object = new Image();
                  image_object.src = $(".smallb").attr("src");
                  
                  native_width = image_object.width;
                  native_height = image_object.height;
                  console.log(native_width , native_width);
                }
                else
                {
                  var magnify_offset = $(this).offset();
                  var mx = e.pageX - magnify_offset.left;
                  var my = e.pageY - magnify_offset.top;
                  
                  if(mx < $(this).width() && my < $(this).height() && mx > 0 && my > 0)
                  {
                    $(".largeb").fadeIn(100);
                  }
                  else
                  {
                    $(".largeb").fadeOut(100);
                  }
                  if($(".largeb").is(":visible"))
                  {
                    var rx = Math.round(mx/$(".smallb").width()*native_width - $(".largeb").width()/2)*-1;
                    var ry = Math.round(my/$(".smallb").height()*native_height - $(".largeb").height()/2)*-1;
                    var bgp = rx + "px " + ry + "px";
                    
                    //Time to move the magnifying glass with the mouse
                    var px = mx - $(".large").width()/2;
                    var py = my - $(".large").height()/2;
                    //Now the glass moves with the mouse
                    //The logic is to deduct half of the glass's width and height from the 
                    //mouse coordinates to place it with its center at the mouse coordinates
                    
                    //If you hover on the image now, you should see the magnifying glass in action
                    $(".largeb").css({left: px, top: py, backgroundPosition: bgp});
                  }
                }
              })
});
      

(function (window, $) {
  
  $(function() {

    $('.ripple').on('click', function (event) {
      event.preventDefault();
      
      var $div = $('<div/>'),
          btnOffset = $(this).offset(),
          xPos = event.pageX - btnOffset.left,
          yPos = event.pageY - btnOffset.top;

      $div.addClass('ripple-effect');
      var $ripple = $(".ripple-effect");
      
      $ripple.css("height", $(this).height());
      $ripple.css("width", $(this).height());
      $div
        .css({
          top: yPos - ($ripple.height()/2),
          left: xPos - ($ripple.width()/2),
          background: $(this).data("ripple-color")
        }) 
        .appendTo($(this));

      window.setTimeout(function(){
        $div.remove();
      }, 2500);
    });
    
  });
  
})(window, jQuery);


jQuery(document).ready(function($){
  var deviceAgent = navigator.userAgent.toLowerCase();
  var agentID = deviceAgent.match(/(iphone|ipod|ipad)/);
  if (agentID) {

        $(".parallax-bg").addClass("parallax-bg-i");
        $(".parallax-bg").removeClass("parallax-bg");
       
 
  }
  setTimeout(function() { $('iframe.delay').attr('src', $('iframe.delay').attr('data-src')); }, 7000);

  if($.browser.mozilla){

      $('.releasing').children().hide();
      $('.releasing_moz').show();

  }

});

    </script>
  </body>
</html>