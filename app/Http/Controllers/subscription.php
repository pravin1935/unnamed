<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Input;

class subscription extends Controller
{
    
    public function register(Request $request){

    		$user = new User;
    		$input =  Input::get('email_subs');
    		$user1 =new User;
    		$mail = $user1->where('email' , $input)->get();
    		if($mail->count())
    		{

					$request->session()->flash('status_error' , 'subscription is already done');
					return redirect('/');
    		}
            $user->email = $input;
    		$user->save();
    		$request->session()->flash('status_done' , 'subscription done - thanks in having interest in our game');
    		return redirect('/');
    }
}
