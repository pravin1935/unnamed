<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class contactus extends Mailable
{
    use Queueable, SerializesModels;
    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $message1 , $email , $name)
    {
        $this->message = $message1;
        $this->email = $email;
        $this ->name = $name;  
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->email , $this->name)
                    ->view( 'mail1')
                    ->with([
                        'message2' => $this->message
                        ]);
                    
    }
}
